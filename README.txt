Autor: Adrián Gutiérrez Camacho (DGME)
Asignatura: Inteligencia Artificial y Estadística (IAE)

El objetivo del trabajo ha sido crear una herramienta de exploración de machine learning que nos permita hacernos una idea rápidamente sobre el conjunto de datos en cuestión que estemos analizando. Con ese objetivo en mente, lo que se ha intentado es ser capaz de generalizar lo máximo posible todas las funciones del mismo, de manera que sirvieran para muchos tipos de conjuntos de datos, y luego se ha debugueado tomando diferentes conjuntos y probando muchas opciones (siempre con sentido).

El presente directorio contiene varios archivos de datos de uso para el programa, un archivo auxiliar de R y el archivo principal .rmd, con el nombre del programa.

Para activar el programa, primero hay que hacer algún cambio en el script (por ejemplo poner y quitar un punto) para que se refresque y se cargue todo correctamente.

Podemos descargar este programa utilizando la siguiente url: https://gitlab.com/adrimaths3997/visualdata.git